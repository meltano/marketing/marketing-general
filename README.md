# Meltano Marketing

:eye: **Public** [issue tracker](https://gitlab.com/meltano/marketing/marketing-general/-/issues) for all things marketing. Use confidential issues as needed.
