## To be included:
### Releases
* Meltano
  * 
* Hub
  * 
* SDK
  * 

### Community Highlights
* 

### In Other News:
* 

---

WIP: https://app.hubspot.com/email/20712484/edit/67210534730/content

Preview: http://meltano-20712484.hs-sites.com/dec-and-jan-dataops-recap-with-meltano?hs_preview=kdznMUpn-67210534730

Publishing Checklist:
- [ ] Match branding as closely as possible
- [ ] Alt text and links on images
- [ ] UTMs on links
- [ ] Finalized and approved content
- [ ] Finalized and approved graphics
- [ ] Finalized and approved design and layout


/label ~"Marketing" ~"Content" 
<!-- Labels to add for triaging -->
~"valuestream::Business Operation" 
~"kind::Non-Product" 
