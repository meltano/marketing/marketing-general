<!-- title issue date (yyyy/mm/Monday date) + social planning
to be opened every Friday to plan out social postings and content for the following week-->

### :loudspeaker: Announcements/ News to Promote  
<!-- Please propose content we share for the week. Note anything that is time-sensitive with relevant dates. Not all content will be selected and some may be used later. Add any notes on the suggested audience for the content if you have it-->
* Office Hours:
* Demo Day:
* New Job Openings:
* 
* 


### :8ball: Themes for the week
<!-- note any relevant themes-->

* 
* 

### :newspaper2: New Media/ PR goign out this week 
<!-- Does this announcement require pitching to journalists or PR? Who should be pitched?-->
* 
* 

### :writing_hand: Copy + links
* 
* 

### :art: Images or video needed
* 
* 

### Social accounts (for social team: make sure all ones we are engaging on that week are checked off)

- [ ] Twitter
- [ ] LinkedIn
- [ ] Banner on home page and Hub
- [ ] Slack announcements
- [ ] Facebook
- [ ] Other

| Platform  | M | T | W | Th | F | SS | Scheduled? | Poster's Initials |
|-----------|---|---|---|----|---|----|------------|------------------|
| Twitter   |   |   |   |    |   |    |            |                  |
| Slack     |   |   |   |    |   |    |            |                  |
| LinekedIn |   |   |   |    |   |    |            |                  |
| Facebook  |   |   |   |    |   |    |            |                  |
| Other     |   |   |   |    |   |    |            |                  |

/assign 
/epic 
/label ~"Marketing" ~"Social" ~"Content" 
<!-- Labels to add for triaging -->
~"valuestream::Business Operation" 
~"kind::Non-Product" 

/due in 7 days
/milestone %[next week]

/confidential
