<!-- This section is to be used by anyone internally requesting Meltano sponsor or participate in an event. Please fill all the necessary information out. If you don’t need a section, feel empowered to delete it from the issue. Remember to always add Related Issues and Epics after you've created your issue so folks have context on what other issues connect to this work. Keep being awesome! 

* title the issue: `"Name of event", Location, Date of Event` (add them remove this line)
* Make the due date the day the event starts
-->

## :bulb: Background/purpose/ Why should we do this?

<!-- Give a sentence overview of why this issue exists. Justification for wanting to sponsor the event and any background you have (please be as detailed as possible). Please note we cannot evaluate any events without context.   -->

## :map: Details and reach

<!-- Include details on why you're completing this work, how it impacts the business, and the potential reach if applicable. Include key dates as well.  -->
* **Official Event Name:** 
* **Date:**
* **Location:** 
* **Event Website:**
* **Expected Number of Attendees at the event:**
* **Meltano Staffing Needs:**
* **Dress Code/ Attire:**
* **Meltano Hosted Landing page (if applicable):**
* **Speakers (if applicable):**
* **Attendee List/ Audience Demographics:**

### :dart: Goals, audience and key messages

<!-- Add context on the goals for this work. Add key messages if applicable. This helps those who may be unfamiliar with your functional group have a better understanding of the work.  -->* **Sponsorship (if applicable):** outline level, benefits and cost breakdown.
* **Event Goals:**
  * Goals for # of leads: 
  * Number of meetings at event: 
  * registration goals/ attendance goals: 

---
### :notebook_with_decorative_cover: Event brief

<!-- outline details of our involvement/ commitment. Be as detailed as possible -->

---
`**For Event Planners complete once event contract signed- this is for our own internal tracking purposes**`

### :construction_site: Pre- Event Checklist (not every item below will be relevant to every show. Delete any items not necessary)
* [ ] Event added to Events Cal and Events page
* [ ] Invoice received
* [ ] Invoice paid
* [Budget Doc]() - Copy template and start new budget for each event
* `Finance Tag(s)` - create the 'campaign tag' (using proper ISOdate_name, noting 37 character limit) then add into the associated budget line item. Each campaign that needs tracking should get its own tag. 
* [ ] Artwork sent- this is for posting on the event page and on-site signage
* [ ] Company description
* [ ] Create and share planning spreadsheet (included travel, booth duty, meetings...) from [planning sheet template]()
* [ ] Slack channel created and attendees invited (Naming convention for channel- use event name, location, and year. link to Epic in channel description)
* [ ] Staffing selected and Tickets allocated   
* [ ] Decide on if we will do swag and any other printed materials (see swag selection below if you are shipping swag)- start swag issue with swag template
* [ ] Attendee directory from organizers (it is not common to get the list of attendees)
* [ ] Press list sent to PR team- create PR issue if needed
* [ ] Social media copy written and scheduled - there is an issue template for this (give them as much notice as possible)
* [ ] Flights/ transport/ lodging booked- added to spreadsheet- use travel template
* [ ] Final prep meeting scheduled (end of week before the show begins. Include everyone attending, planning and person from PMM who will do demo training)
* [ ] Event post mortem scheduled (1-2 weeks post event)
* [ ] Post event feedback survey created- to be shared post event in meta issue.
* [ ] Event recap (week after) & Feedback collected.

## :performing_arts: Booth/Theatre
* [ ]  Booth Design/ Virtual Booth Issue created
* [ ]  Positioning and Messaging confirmed
* [ ]  [AV ordered- monitors] `(!add conf number)`
* [ ]  [Electric ordered & conf number] `(!add conf number)`
* [ ]  [Screen and HDMI cable ordered] `(!add conf number)`
* [ ]  [Furnishing ordered (Countertop, Stools, Coffee table)] `(!add conf number)`
* [ ]  Booth staff scheduled- add link to schedule in staffing issue
* [ ]  Booth slide deck created/ decided on
* [ ]  Share booth deck on slack channel- best to share published, shortened url
* [ ]  Click through demo setup (iPad)
* [ ]  Power if not incuded in package or need more
* [ ]  Final mock up approved
* [ ]  Booth Artwork issue created and artwork submitted - link to issue

### :iphone: Lead scanning
Badge scanners- get 1 for around every 1000 people at the show, but do not exceed the amount of people we have staffing the booth at one time. 
* [ ]  Lead Scanner / App ordered: `qty`
* [ ]  [Lead Scanner conf number] `(!add conf number)`
* [ ]  Lead license assigned: @username

Assign to DRI! 
Add due date as event Date.

/label ~"Marketing" ~"Event"

/assign @emily
