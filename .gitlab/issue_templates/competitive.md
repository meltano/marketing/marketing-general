<!-- title issue Name of Competitor + Competitive Analysis-->

### HIgh level info 
<!-- Please propose content we share for the week. Note anything that is time-sensitive with relevant dates. Not all content will be selected and some may be used later. Add any notes on the suggested audience for the content if you have it-->
* URL:
* Year founded/ years in market:
* key tag line:
* Open source?
* Funding status:

### Overview of high level messaging/ positioning
<!-- note high level messaging-->

* 
* 

### What are they doing well
<!-- note any things they are doing well-->

* 
* 

### Weak spots
<!-- What are their weakest points?-->
* 
* 

### Where do we overlap
* 
* 

### :art: Images or videos
* 
* 


/assign 
/epic 
/label ~"Marketing"
<!-- Labels to add for triaging -->
~"valuestream::Business Operation" 
~"kind::Non-Product" 

/due in 7 days
/milestone %[next week]

/confidential
