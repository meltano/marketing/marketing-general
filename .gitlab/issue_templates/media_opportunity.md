>>>

Show Notes: 

Show overview and link
>>>

## Recording information:

- Recording link
- Host
- Contact

## Getting the most out of every episode/ process:
Once booked the following checklist needs to happen...
- [x] send cal invite for podcast and add Emily and Amanda to the invite so we know when recording is happening and we can plan social 
- [ ] communicate to marketing launch date and when we will get video. transcript (add cal invite whenever possible)
- [ ] possible strategy call before recording to discuss key positioning, points, and CTA

#### on podcast
- [ ] push CTA and key points
- [ ] if it is a potential investor sourcing opportunity... "Mention in the interview we are preparing to raise a round of funding and describe out ideal investor relationship and how much money you need."
- [ ] thanks the host and let them know we will be promoting the episode and ask if there is anything else we can do to help
- [ ] ask when it will go live
- [ ] if there is a connection say you would like to stay in touch if possible (needs to be followed up with a personal email a couple of days later)

#### for marketing post podcast
- [ ] schedule and craft social plans (must tag host and podcast name in posts)
- [ ] Collect video snippets and any relevant artwork 
- [ ] collect key quotes for promotion
- [ ] craft plan for continued promotion and content creation opportunities
   - [ ] repurpose video/ audio
   - [ ] transcribe text
   - [ ] create blog posts/ newsletter from content 
   - [ ] create PR pitch for future


/label ~"Marketing" ~"Content" 
/assign @emily
<!-- Labels to add for triaging -->
~"valuestream::Business Operation" 
~"kind::Non-Product" 
