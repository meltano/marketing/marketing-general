## Todos

- [ ] Monday: Add this ticket to Kickoff doc
- [ ] Monday: Source demos from the community and confirm Meltano's demos with Engineering/Product
- [ ] Monday: Draft Tweets for the week
   - [ ] For Monday or Tuesday: Announce time, mention a specific demo, and encourage folks to join us on Slack.
   - [ ] For Thursday an hour before DD: Announce that demo day is in one hour, encourage people to come see us, inform that we'll post the YouTube link soon
   - [ ] For Thursday when we go live: Create a draft announcing that we're live with a placeholder for the YouTube Link.
- [ ] Monday: Draft Slack Announcements and schedule
  - [ ] Monday or Tuesday: Post to #demo-day and "Share" post to #announcements
  - [ ] Thursday an hour before DD: Post to #demo-day that we start in an hour.
  - [ ] Thursday when we go live: `@channel` mention that we're live and post a link to the Zoom URL
- [ ] Before: Create/clean up [Demo Day deck](https://docs.google.com/presentation/d/1Pp56ORBQeFd6JvLuzIERr0GaSps5n2fJmjxvd5mmP3E/edit?usp=sharing)
- [ ] Thursday - 1hr before: Double check that announcements were posted to #demo-day and #announcements
- [ ] Thursday: Set up stream per instructions here: https://meltano.com/handbook/marketing/community.html#streaming-from-zoom-to-youtube
- [ ] Thursday: Tweet out when YouTube stream is live (if you can't, make sure someone is on deck to tweet)
- [ ] Thursday: Post Zoom link to #demo-day/#announcements Slack channels when stream is live (if you can't, make sure someone is on deck to post)
- [ ] Thursday: Edit live stream video per instructions here: https://meltano.com/handbook/marketing/community.html#after-the-livestream-session-ends
- [ ] Thursday: Thank community folks who joined the call with us in #demo-day
- [ ] Thursday: Write recap blog post with highlights from Demo Day
- [ ] Thursday: Open next week's issue.
- [ ] Thursday: Tag YouTube video with appropriate tags
- [ ] Friday: Tweet a link to the blog post during peak hours

/assign @afolson
/label ~"Marketing" 

/due in 5 days
