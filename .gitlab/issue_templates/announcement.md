#Announcement 
"Insert Blog overview here". What type of post is thi and who is it for?
Issue name: "Name of blog Post"- blog

### Writing guide
(See copy template](https://docs.google.com/document/d/1s9UsxZ7igSCMYudi4oFOI5LoftqDOKkFCgrxRuHbBNY/edit#) - make a copy to use
Please write in word so we can collab on editng 

### Keywords

- [ ] XYZ
- [ ] XYZ
- [ ] XYZ

### Key article attributes

- [ ] Title
- [ ] url
- [ ] meta description
- [ ] Optimized headings
- [ ] Includes images and alt tags for them
- [ ] includes keywords

/assign 
/epic 
/label ~"Marketing" ~"Social" ~"Content" 
<!-- Labels to add for triaging -->
~"valuestream::Business Operation" 
~"kind::Non-Product" 

/due in 7 days
/milestone %[next week]

/confidential
