### Todos

- [ ] Monday: [Check board](https://gitlab.com/groups/meltano/-/boards/2923184) and add this ticket to Kickoff doc
- [ ] Monday: Confirm topics with Engineering/Product
- [ ] Monday: Draft Tweets for the week
   - [ ] For Monday or Tuesday: Announce office hours time, link an issue that we're discussing, and encourage folks to join us on Slack.
   - [ ] For Wednesday an hour before OH: Announce that office hours are in one hour, encourage people to come see us, inform that we'll post the YouTube link soon
   - [ ] For Wednesday when we go live: Create a draft announcing that we're live with a placeholder for the YouTube Link.
- [ ] Monday: Draft Slack Announcements and schedule
  - [ ] Monday or Tuesday: Post to #office-hours and "Share" post to #announcements
  - [ ] Wednesday an hour before OH: Post to #office-hours that we start in an hour.
  - [ ] Wednesday when we go live: `@channel` mention that we're live and post a link to the Zoom URL
- [ ] Before: Create/clean up Office Hours deck
- [ ] Wednesday - 1hr before: Double check that announcements were posted to #office-hours and #announcements
- [ ] Wednesday: Set up stream per instructions here: https://meltano.com/handbook/marketing/community.html#streaming-from-zoom-to-youtube
- [ ] Wednesday: Tweet out when YouTube stream is live (if you can't, make sure someone is on deck to tweet)
- [ ] Wednesday: Post Zoom link to #office-hours/#announcements Slack channels when stream is live (if you can't, make sure someone is on deck to post)
- [ ] Wednesday: Edit live stream video per instructions here: https://meltano.com/handbook/marketing/community.html#after-the-livestream-session-ends
- [ ] Wednesday: Thank community folks who joined the call with us in #office-hours
- [ ] Wednesday: Open next week's Office Hours issue.
- [ ] Wednesday: Tag YouTube video with appropriate tags

/assign @afolson
/label ~"Marketing" 

/due in 3 days
