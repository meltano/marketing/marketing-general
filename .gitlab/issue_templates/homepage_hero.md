### Announcement 
(Summarize the announcement. This issue is marked confidential by default just in case the announcement is embargoed. )

### Target Date
(Who is this announcement for? Different audiences may require different copy.)

### Requied art
* Artwork size

### CTA and Copy
* add copy for banner

#### Sources
* [Add banner source](https://gitlab.com/meltano/meltano/-/commit/8378c835fabd0722441f03e3f81baaa7bd6ae97b)
* [Remove banner Source](https://gitlab.com/meltano/meltano/-/commit/ceb6ce351f9a3e4f48f3ead5498b5259b766216d)

### Todos

- [ ] Update homepage banner
- [ ] (Optional) Update Hub banner
- [ ] (Optional) Update Handbook banner
